import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'

Vue.use(Vuex)

// 使用localStorage 用try catch来包裹起来,
// 避免用户将本地的localStorage关闭或隐藏不显示导致页面显示错误,此代码被分割到state.js中

export default new Vuex.Store({ 
	state,
	mutations
	// getters: {
	// 	doubleCity (state) {
	// 		return state.city + '' + state.city
	// 	}
	// }

	// state: {
	// 	// city: '广州'
	// 	city: defaultCity
	// },
	// actions: {
	// 	changeCity (ctx, city) {
	// 		ctx.commit('changeCity', city)
	// 	}
	// },
	// acitons可以不用，组件直接调用mutations
	// mutations: {
	// 	changeCity (state, city) {
	// 		state.city = city
	// 		try {
	// 			localStorage.city = city
	// 		} catch (e) {}
	// 	}
	// }
})